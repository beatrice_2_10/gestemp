
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

@WebServlet("/gi")
public class IngenierosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DataSource dataSource;

	@Override
	public void init(ServletConfig config) throws ServletException {

		try {
			InitialContext context = new InitialContext();
			dataSource = (DataSource) context.lookup("java:comp/env/jdbc/ge");
			if (dataSource == null) {
				throw new ServletException("Error");
			}
		} catch (NamingException e) {
			Logger.getLogger(IngenierosServlet.class.getName()).log(Level.SEVERE, null, e);

		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession sesion = request.getSession();

		// String usuario= (String)sesion.getAttribute("usuario");//obtener variable de
		// sesión
		// sesion.setAttribute("usuario", "bea");//crearla o actualizarla

		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();

		HTML5.comienzo(out, "Gestión de Empleados");
		out.println("<h1>Dar de alta a un ingeniero</h1>");
		out.println("<form method=\"GET\" action=\"gi\">");
		out.println("<p>Nombre:<input type=\"text\" name=\"nombre\" required=\"required\" autofocus></input>");
		out.println("<p>Num NSS:<input type=\"text\" name=\"nss\" required=\"required\"></input>");
		out.println("<p>Salario:<input type=\"number\" name=\"sal\" required=\"required\"></input>");
		out.println("<p>Especialidad:<input type=\"text\" name=\"esp\" required=\"required\"></input>");
		out.println("<p><label for=\"dep\" required=\"required\">Departamento </label>");
		out.println("<select name=\"dep\">");
		agregarDepartamentos(out);
		out.println("</select>");
		out.println("<p><input type=\"submit\" ></input>");
		out.println("</form>");

		insertarEmple(out, request);
		HTML5.fin(out);

	}

	private void agregarDepartamentos(PrintWriter out) {
		try {
			Connection c = dataSource.getConnection();
			Statement sql = c.createStatement();
			ResultSet rs = sql.executeQuery("select nombre from departamentos;");
			while (rs.next()) {
				out.printf("<option>%s</option>\n", rs.getString("nombre"));

			}
			rs.close();
			c.close();
		} catch (SQLException e) {

		}

	}

	private void insertarEmple(PrintWriter out, HttpServletRequest request) {

		try {
			Connection c = dataSource.getConnection();
			String usu = request.getParameter("nombre");
			String nss = request.getParameter("nss");
			String sal = request.getParameter("sal");
			String dep = request.getParameter("dep");
			String esp = request.getParameter("esp");
			PreparedStatement sql = c.prepareStatement("insert into empleados values(?,?,?)");
			sql.setString(1, nss);
			sql.setString(2, usu);
			sql.setString(3, sal);

			sql.executeUpdate();

			PreparedStatement sql1 = c.prepareStatement("insert into ingenieros values(?,?)");
			sql1.setString(1, nss);
			sql1.setString(2, dep);

			sql1.executeUpdate();

			PreparedStatement sql2 = c.prepareStatement("insert into especialidades values(?,?)");
			sql2.setString(1, nss);
			sql2.setString(2, esp);

			sql2.executeUpdate();

			out.print("¡Ingeniero insertado!");

		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

}
